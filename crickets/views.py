from django.shortcuts import render
from crickets.models import *
from django.db.models import Count, Sum, Q
from django.views import generic
from django.forms import ModelForm
from django.http import HttpResponseRedirect, HttpResponse
import datetime
from datetime import timedelta
from django.utils import translation
from django.core.exceptions import ObjectDoesNotExist
from crickets.forms import *
from django.views.generic.edit import CreateView

# Create your views here.

def holding(request):
    return render(request, 'crickets/holding.html', {})

def index(request):
    context={}
    request.session["exhib"] = False
    context['done_training'] = False
    # don't add anything to session till player has passed check
    if 'done_training' in request.session:
        context['done_training'] = request.session["done_training"]
        
    return render(request, 'crickets/index.html', context)

def index_exhib_reset_lang(request):
    # on installation version - clear the session stuff here...
    request.session.flush()
    request.session["exhib"] = True
    context={}
    context['done_training'] = False
    # don't add anything to session till player has passed check
    if 'done_training' in request.session:
        context['done_training'] = request.session["done_training"]
        
    return render(request, 'crickets/index.html', context)

def index_exhib(request):
    # on installation version - clear the session stuff here...
    lang = False
    if translation.LANGUAGE_SESSION_KEY in request.session:
        lang = request.session[translation.LANGUAGE_SESSION_KEY]
    request.session.flush()
    if lang: request.session[translation.LANGUAGE_SESSION_KEY]=lang
    request.session["exhib"] = True
    context={}
    context['done_training'] = False
    # don't add anything to session till player has passed check
    if 'done_training' in request.session:
        context['done_training'] = request.session["done_training"]
        
    return render(request, 'crickets/index.html', context)


def about(request):
    return render(request, 'crickets/about.html', {})

def check(request):
    return render(request, 'crickets/check.html', {})

def training(request):
    return render(request, 'crickets/training.html', {})

def choose(request):
    # can only have got here via training or already done with check passed...

    request.session["done_training"]=True

    # first time here?
    if 'player_number' not in request.session:
        # normal online player?
        if 'group_id' not in request.session:
            exhib = 0
            # at the moment just eden, but could have others...
            if request.session["exhib"]: exhib=1
            player = Player(name = "???", videos_watched = 0, exhib = exhib)
            player.save()
            request.session["player_number"]=player.id
        else: 
            # we have a group_id so we are in the classroom version           
            player = Player(name = "???", videos_watched = 0, exhib = 2)
            player.save()
            # connect us to signed in class group
            PlayerToClassGroup(
                player=player,
                class_group=ClassGroup.objects.get(group_id=request.session["group_id"])).save()
            request.session["player_number"]=player.id
    else:
        if 'group_id' in request.session:
            # we have an existing player using the class version
            player = Player.objects.get(pk=request.session["player_number"])
            # make sure we are marked as classgroup player
            if player.exhib!=2:
                player.exhib = 2
                player.save()
    
    context = {}
    # only show ones with enough videos currently ready
    # and order by reverse activity (activity = total number of 
    # unique viewers on videos currently avalable)
    context['crickets'] = Cricket.objects.exclude(videos_ready__lt=5).order_by('activity')[:6]

    # blame git for this...
    for cricket in context['crickets']:
        if cricket.tag=="+1": cricket.tag="Plus1"
        if cricket.tag=="+7": cricket.tag="Plus7"
        if cricket.tag=="+9": cricket.tag="Plus9"
        if cricket.tag=="+6": cricket.tag="Plus6"
        if cricket.tag=="+A": cricket.tag="PlusA"
        if cricket.tag=="+C": cricket.tag="PlusC"
        if cricket.tag=="+E": cricket.tag="PlusE"
        if cricket.tag=="+=": cricket.tag="PlusEqual"

    return render(request, 'crickets/choose.html', context)

class CricketView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/play.html'
    def get_context_data(self, **kwargs):
        context = super(CricketView, self).get_context_data(**kwargs)
        # random selection of currently active videos, should 
        # be ok as we only have quite a low total available at one time.
        context['movies'] = Movie.objects.filter(cricket=context['cricket']).filter(status=1).order_by('?')[:5]
        context['path'] = str(context['movies'][0].season)+"/"+context['movies'][0].camera

        # check using the session to see where we need to go
        # after this - keyboard or personality
        context['done_keyboard']=False
        context['player_number']=-1
        if 'player_number' in self.request.session:
            player = Player.objects.get(pk=self.request.session["player_number"])
            context["player_id"]=self.request.session["player_number"]
            # turn off keyboard for class players
            if player.name != "???" or "group_id" in self.request.session:
                context['done_keyboard']=True

        return context 

def avg_time(datetimes):
    total = sum(dt.hour * 3600 + dt.minute * 60 + dt.second for dt in datetimes)
    return total / len(datetimes)

def score(score, min_score, max_score):
    ret = ((score-min_score)/(max_score-min_score))*100
    if ret>100: return 100
    if ret<0: return 0
    return ret

class PersonalityView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/personality.html'
    def get_context_data(self, **kwargs):
        context = super(PersonalityView, self).get_context_data(**kwargs)
        # need to calculate the cricket data *here* as 
        # these need to include the player's data just
        # saved - not rely on the robot update
        cricket = context['cricket']
        num_videos = float(Movie.objects.filter(cricket=cricket,status__gt=0).count())
        context['eating_score'] = score(Event.objects.filter(movie__cricket=cricket,event_type='eating').count()/num_videos,
                                        Value.objects.filter(name='eating_min')[0].value,
                                        Value.objects.filter(name='eating_max')[0].value)
        context['singing_score'] = score(Event.objects.filter(movie__cricket=cricket,event_type='singing').count()/num_videos,
                                         Value.objects.filter(name='singing_min')[0].value,
                                         Value.objects.filter(name='singing_max')[0].value)
        context['moving_score'] = score(Event.objects.filter(movie__cricket=cricket).filter(Q(event_type="in")|Q(event_type="mid")|Q(event_type="out")).count()/num_videos,
                                        Value.objects.filter(name='moving_min')[0].value,
                                        Value.objects.filter(name='moving_max')[0].value)

        cricket.tag2 = cricket.tag

        # blame git for this...
        if cricket.tag=="+1": cricket.tag2="Plus1"
        if cricket.tag=="+7": cricket.tag2="Plus7"
        if cricket.tag=="+9": cricket.tag2="Plus9"
        if cricket.tag=="+6": cricket.tag2="Plus6"
        if cricket.tag=="+A": cricket.tag2="PlusA"
        if cricket.tag=="+C": cricket.tag2="PlusC"
        if cricket.tag=="+E": cricket.tag2="PlusE"
        if cricket.tag=="+=": cricket.tag2="PlusEqual"
        
        # keeping cricket.daynight_score just in case this
        # becomes too slow...
        times = []
        for event in Event.objects.filter(movie__cricket=cricket):
            times.append(event.estimated_real_time)
        if len(times)>0:
            a = avg_time(times)
            context['daynight_score']=(a/(60.0*60.0*24))*100

        return context 

class ResultsMovementView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/results_movement.html'
    def get_context_data(self, **kwargs):
        context = super(ResultsMovementView, self).get_context_data(**kwargs)
        cricket = context['cricket']

        times = []
        for event in Event.objects.filter(movie__cricket=cricket).filter(Q(event_type="in")|Q(event_type="mid")|Q(event_type="out")):
            times.append(event.estimated_real_time)
        if len(times)>0:
            a = avg_time(times)
            context['moving_score']=(a/(60.0*60.0*24))*80
        else:
            context['moving_score']=-1000

        return context 

class ResultsEatingView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/results_eating.html'
    def get_context_data(self, **kwargs):
        context = super(ResultsEatingView, self).get_context_data(**kwargs)
        cricket = context['cricket']
        
        times = []
        for event in Event.objects.filter(movie__cricket=cricket,event_type="eating"):
            times.append(event.estimated_real_time)
        if len(times)>0:
            a = avg_time(times)
            context['eating_score']=(a/(60.0*60.0*24))*80
        else:
            context['eating_score']=-1000

        return context 

class ResultsSingingView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/results_singing.html'
    def get_context_data(self, **kwargs):
        context = super(ResultsSingingView, self).get_context_data(**kwargs)
        cricket = context['cricket']

        times = []
        for event in Event.objects.filter(movie__cricket=cricket,event_type="singing"):
            times.append(event.estimated_real_time)
        if len(times)>0:
            a = avg_time(times)
            context['singing_score']=(a/(60.0*60.0*24))*80
        else:
            context['singing_score']=-1000

        return context 

class KeyboardView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/keyboard.html'
    def get_context_data(self, **kwargs):
        context = super(KeyboardView, self).get_context_data(**kwargs)
        
        context['num_videos']=0
        if 'player_number' in self.request.session:
            player = Player.objects.get(pk=self.request.session["player_number"])
            context['num_videos']=player.videos_watched
            context["player_id"]=player.id

        return context 

def player_name(request):
    if request.method == 'POST':
        if 'player_number' in request.session:
            player = Player.objects.get(pk=request.session["player_number"])
            player.name=request.POST['name'][:3]
            player.save()
    return HttpResponse('')

class LeaderboardView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/leaderboard.html'
    def get_context_data(self, **kwargs):
        context = super(LeaderboardView, self).get_context_data(**kwargs)
        context["player_list"]=Player.objects.exclude(name="???").exclude(videos_watched=0).order_by('-videos_watched')[:13]
        return context

## incoming from javascript...
def record_event(request):
    if request.method == 'POST':
        # concerned about how much overhead is involved
        # with this, as it's generating an option list entry
        # for each movie - seen when you print (but perhaps they
        # are not actually created until printing)
        form = EventForm(request.POST)
        # probably not even needed, but who knows where these may
        # be coming from eventually...?
        if form.is_valid():

            obj = Event() #gets new object
            obj.movie = form.cleaned_data['movie']
            obj.event_type = form.cleaned_data['event_type']
            obj.user = form.cleaned_data['user']
            obj.video_time = form.cleaned_data['video_time']
            obj.x_pos = form.cleaned_data['x_pos']
            obj.y_pos = form.cleaned_data['y_pos']
            obj.other = form.cleaned_data['other']

            # calculate dodgy time estimation based on proportion
            # through screen video and move start/end time            
            movie = obj.movie            
            screen_length_secs = 30
            t = obj.video_time/screen_length_secs
            diff = movie.end_time-movie.start_time            
            realtime = movie.start_time+timedelta(seconds=diff.total_seconds()*t)
            obj.save()
            # for some reason need to re-save this
            obj.estimated_real_time = realtime
            obj.save()
            
            # get the the cricket 
            cricket = Cricket.objects.get(pk=movie.cricket.id)            
                
            if obj.event_type=="burrow_start":
                mv = MovieView.objects.create(viewer=Player.objects.get(pk=request.session["player_number"]),
                                              movie=movie)
                mv.save()
                movie.views+=1
                movie.unique_views = MovieView.objects.filter(movie=movie).values("viewer").distinct().count()
                movie.save()
                # activity for a cricket is the total unique views 
                # across all currently active videos videos - this 
                # prioritises crickets viewed by fewest different people
                # (perhaps something we can move to update?)
                cricket.activity=Movie.objects.filter(cricket=cricket,status=1).aggregate(Sum('unique_views'))['unique_views__sum']  

            if obj.event_type=="cricket_end" or obj.event_type=="no_cricket_end":
                # on burrow_start update player videos watched
                if "player_number" in request.session:
                    player = Player.objects.get(pk=request.session["player_number"])
                    player.videos_watched+=1
                    player.save()
                
            cricket.save()
            return HttpResponse('')
        print(form.errors)
        return HttpResponse('request is invalid: '+str(form))
    else:
        form = EventForm()
        return render(request, 'crickets/event.html', {'form': form})

def login(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = LoginForm(request.POST)
        # check whether it's valid:
        if form.is_valid():            
            group_id = form.cleaned_data["group_id"]
            try:
                cg = ClassGroup.objects.get(group_id=group_id)
                request.session["group_id"]=group_id
                return HttpResponseRedirect('/classgroup_check/')
            except ObjectDoesNotExist:
                return HttpResponseRedirect('/error/')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = LoginForm()

    return render(request, 'crickets/login.html', {'form': form})

def classgroup_check(request):
    cg = ClassGroup.objects.get(group_id=request.session["group_id"])
    context = {}
    context["school_name"] = cg.school_name
    context["class_name"] = cg.class_name    
    return render(request, 'crickets/classgroup_check.html', context)

def error(request):
    return render(request, 'crickets/error.html', {})
    
def welcome(request):
    cg = ClassGroup.objects.get(group_id=request.session["group_id"])
    context = {}
    context["school_name"] = cg.school_name
    context["class_name"] = cg.class_name    
    return render(request, 'crickets/welcome.html', context)
    
class ClassGroupCreateView(CreateView):
    model = ClassGroup
    form_class = ClassGroupForm
    def get_success_url(self):
        return "/login/"

def make_hist():
    return [0,0,0,0,0,0,0,0,0,0,0,0]

def hist_add(t,hist):
    hist[int(t.hour/2)]+=1

def collect_hists(event,player_id,group_id,hists):
    t = event.estimated_real_time.time()

    # is there a link between the player who made the event
    # and the current class group?            
    if len(PlayerToClassGroup.objects.filter(player=event.user.id, class_group__group_id=group_id))>0:
        hist_add(t,hists[1])

    if event.user.id == player_id:
        hist_add(t,hists[2])       

# shifts all the data so 6-8 am is the first bar
def shuffle6am(hists):
    ret = []
    for data in hists:
        d = []
        for hist in data:
            d.append(hist[3:]+hist[:3])
        ret.append(d)
    return ret

class ResultsClassView(generic.DetailView):
    model = Cricket
    template_name = 'crickets/results_class.html'
    def get_context_data(self, **kwargs):
        context = super(ResultsClassView, self).get_context_data(**kwargs)
        cricket = context['cricket']

        player_id = self.request.session['player_number']
        # shouldn't be able to see this page without one
        group_id = self.request.session['group_id']

        # hardcoded total results before the class version
        hists = [[[188, 220, 247, 599, 1607, 4175, 7383, 8797, 5764, 1740, 2141, 1087],make_hist(),make_hist()],
                 [[20, 11, 22, 113, 490, 1052, 1862, 3222, 1411, 151, 99, 108],make_hist(),make_hist()],
                 [[34, 35, 31, 176, 517, 1200, 2191, 2894, 1239, 203, 300, 273],make_hist(),make_hist()]]


        # player only results
        for event in Event.objects.filter(user=player_id).filter(Q(event_type="in")|Q(event_type="mid")|Q(event_type="out")):
            hist_add(event.estimated_real_time.time(),hists[0][2])
        for event in Event.objects.filter(user=player_id).filter(Q(event_type="eating")):
            hist_add(event.estimated_real_time.time(),hists[1][2])
        for event in Event.objects.filter(user=player_id).filter(Q(event_type="singing")):
            hist_add(event.estimated_real_time.time(),hists[2][2])
            
        p2g = PlayerToClassGroup.objects.filter(player=player_id)
        if len(p2g)>0:
            # just check first group
            players = PlayerToClassGroup.objects.filter(class_group=p2g[0].class_group)
            # get players from our group
            events = Event.objects.filter(
                user__in=[v['player_id']
                          for v in list(players.values('player_id'))])
            
            # results from our class group
            for event in events.filter(Q(event_type="in")|Q(event_type="mid")|Q(event_type="out")):
                hist_add(event.estimated_real_time.time(),hists[0][1])
            for event in events.filter(Q(event_type="eating")):
                hist_add(event.estimated_real_time.time(),hists[1][1])
            for event in events.filter(Q(event_type="singing")):
                hist_add(event.estimated_real_time.time(),hists[2][1])

        context["hists"]=shuffle6am(hists)
        return context 


# todo
# fix axis label cutoff
