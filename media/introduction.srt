1
00:00:00,770 --> 00:00:02,810
<font  >Hello and welcome to Cricket Tales.</font>

2
00:00:03,200 --> 00:00:11,060
<font  >My name's Tom Tregenza, and I'm a professor of evolutionary ecology at the University of Exeter's campus in Cornwall.</font>

3
00:00:11,990 --> 00:00:15,170
<font  >My job is to study evolution.</font>

4
00:00:15,860 --> 00:00:25,820
<font  >Evolution is very important, mainly because that is why the whole of the living world is like it is and is in existence.</font>

5
00:00:25,820 --> 00:00:31,970
<font  >It has evolved from very simple life forms into the incredible range of animals and plants we see today.</font>

6
00:00:32,420 --> 00:00:40,399
<font  >Evolution is also important because it helps us understand what might happen in the future if the world changes.</font>

7
00:00:40,400 --> 00:00:44,209
<font  >And we know that the world is changing because the climate is warming up.</font>

8
00:00:44,210 --> 00:00:47,270
<font  >So we need to understand evolution to predict what's going to happen.</font>

9
00:00:48,260 --> 00:00:55,310
<font  >And the this might sound a bit weird, but the way that I study evolution is to study crickets...</font>

10
00:00:55,700 --> 00:01:00,590
<font  >Now, as you probably know, crickets are insects  a little bit like grasshoppers.</font>

11
00:01:00,590 --> 00:01:04,910
<font  >They're very closely related to grasshoppers and bush crickets, which we have lots of in the United Kingdom.</font>

12
00:01:05,120 --> 00:01:08,240
<font  >We have crickets here, too, but you don't see them quite as often.</font>

13
00:01:08,240 --> 00:01:13,399
<font  >They tend to be dark and they live on the ground and they often have burrows.</font>

14
00:01:13,400 --> 00:01:19,520
<font  >The ones that we study have burrows. But because they're not so common in Britain, we study them in northern Spain.</font>

15
00:01:20,300 --> 00:01:25,129
<font  >The reason that we can study crickets in order to understand important things about how</font>

16
00:01:25,130 --> 00:01:32,180
<font  >evolution works is because the way that evolution works is very similar across all species.</font>

17
00:01:32,930 --> 00:01:39,050
<font  >So for instance, if the climate changes and let's say it gets a bit hotter in the middle of the day,</font>

18
00:01:39,080 --> 00:01:44,900
<font  >then it might be good for crickets to avoid being quite so active in the middle of the day when it's really hot.</font>

19
00:01:45,350 --> 00:01:51,800
<font  >So if some crickets have got genes, (which are the little codes that we have inside us that build our body),</font>

20
00:01:52,190 --> 00:01:58,970
<font  >if some crickets have got genes that predispose them (meaning that they're more likely) to get up early in the morning,</font>

21
00:01:59,570 --> 00:02:04,820
<font  >then those crickets might tend to do a bit better if the climate warms up because they get up early in the morning.</font>

22
00:02:04,820 --> 00:02:09,980
<font  >They are very busy at that time, and then they can avoid being quite so busy later in the day.</font>

23
00:02:10,280 --> 00:02:17,330
<font  >And then what you might find is that those early-rising crickets have more offspring, more babies.</font>

24
00:02:17,360 --> 00:02:22,280
<font  >Those babies also carry those genes that say "get up early in the morning".</font>

25
00:02:22,490 --> 00:02:26,959
<font  >And so you have more "get up early in the morning" genes in the population.</font>

26
00:02:26,960 --> 00:02:33,530
<font  >And what's happened there is the population has evolved to include more individuals that get up early in the morning.</font>

27
00:02:34,100 --> 00:02:40,130
<font  >But all of this requires that there's variation in the population, genetic variation.</font>

28
00:02:40,160 --> 00:02:45,170
<font  >These genes, for what time you get up in the morning, have to have different types.</font>

29
00:02:45,470 --> 00:02:47,240
<font  >And that's what we're going to look at today.</font>

30
00:02:48,650 --> 00:02:56,090
<font  >That and some other possible differences in behaviour: sources of variation in behaviour between individuals.</font>

31
00:02:56,480 --> 00:03:02,650
<font  >So what we've been doing, to try to study this, is in a meadow in northern Spain in a secret location</font>

32
00:03:02,650 --> 00:03:08,570
<font  >and we have to keep it secret because we've got 140 video cameras in there and it's a meadow</font>

33
00:03:08,570 --> 00:03:15,530
<font  >so we can't keep it all secure. And in that field, we've got little tags attached to every cricket that we can find.</font>

34
00:03:15,800 --> 00:03:20,029
<font  >One of the reasons we like crickets is because they build these burrows and they</font>

35
00:03:20,030 --> 00:03:24,770
<font  >run into their burrow if they think they're going to be attacked by a predator.</font>

36
00:03:25,460 --> 00:03:29,510
<font  >So we know where they're going to be and we can set up cameras next to these burrows.</font>

37
00:03:29,660 --> 00:03:34,879
<font  >And we've recorded over a million hours of video of all these different crickets in this meadow.</font>

38
00:03:34,880 --> 00:03:41,720
<font  >And there are hundreds of crickets in the meadow, and all of them have got their own unique little tag: number, letter combination on them.</font>

39
00:03:42,950 --> 00:03:48,830
<font  >And what we need is your help to look at the variation in these crickets in this meadow.</font>

40
00:03:48,980 --> 00:03:53,750
<font  >So what you're going to be doing is looking at some of the video that we've collected in our meadow,</font>

41
00:03:53,780 --> 00:03:58,279
<font  >and this is video that has probably never been watched by anyone before.</font>

42
00:03:58,280 --> 00:04:01,730
<font  >You will be the first people looking at this video,</font>

43
00:04:02,660 --> 00:04:10,160
<font  >and what you'll do is watch the videos and record some things about the behaviour of the crickets.</font>

44
00:04:10,190 --> 00:04:12,890
<font  >Now quite often, the cricket is going to be down its burrow.</font>

45
00:04:12,960 --> 00:04:17,420
<font  >That's going to be a very easy video to watch because you'll just go, okay, I didn't see a cricket.</font>

46
00:04:17,690 --> 00:04:24,500
<font  >Other times you'll see the cricket and you'll need to record its behaviour and there's  instructions that you're going to get about how to do that.</font>

47
00:04:25,070 --> 00:04:32,960
<font  >So with this work you're helping us to look at how much variation there is and helping us to understand how evolution works.</font>

48
00:04:33,080 --> 00:04:37,280
<font  >So thank you very much for your help, and enjoy cricket tales.</font>

