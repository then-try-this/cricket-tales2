var myChart = false;
var data = false;
var cols = false;
var labels = false;
var hists = false;
var datatype = 0;

function find_max(data) {
    let max = 0;
    for (let d of data) {
	    if (d>max) max=d;
    }
    return max;
}

function show(e) {
    let dataset=0;
    if (e.target.id=="show_0") dataset=0;
    if (e.target.id=="show_1") dataset=1;
    if (e.target.id=="show_2") dataset=2;
    myChart.data.datasets[0].data = data[dataset];
    myChart.data.datasets[0].backgroundColor = cols[dataset];
    myChart.data.datasets[0].label = labels[dataset];


    let max = 9000 // moving
    if (datatype==1) max=4000; // eating 
    if (datatype==2) max=3000; // singing
    
    if (dataset==0) myChart.config.options.scales.y.max = max; 
    else myChart.config.options.scales.y.max = find_max(data[1]);

//    if (dataset==0) myChart.config.options.scales.y.suggestedMax = 9000; // find_max(data[0]);
 //   else myChart.config.options.scales.y.suggestedMax = find_max(data[1]);

    
    myChart.update();
}

function type(e) {
    if (e.target.id=="type_0") datatype=0;
    if (e.target.id=="type_1") datatype=1;
    if (e.target.id=="type_2") datatype=2;
    data = hists[datatype]
    myChart.data.datasets[0].data = data[2];
    myChart.data.datasets[0].backgroundColor = cols[2];
    myChart.data.datasets[0].label = labels[2];
    myChart.config.options.scales.y.max = find_max(data[1]);
    myChart.update();
    
/*    let max = 9000; // moving
    if (datatype==1) max=4000; // eating 
    if (datatype==2) max=3000; // singing
    
    myChart.config.options.scales.y.max = max;*/
    myChart.update();

    document.getElementById('y-axis').innerHTML="How many times the crickets "+["moved","ate","sang"][datatype];
    document.getElementById('graph-title').innerHTML="Research results : "+["movement","eating","singing"][datatype];
}


function setup_class_graphs(new_hists) {

    hists = new_hists;    
    data = hists[0];
    
    const ctx = document.getElementById('myChart').getContext('2d');

    document.getElementById('show_0').addEventListener("click", show);
    document.getElementById('show_1').addEventListener("click", show);
    document.getElementById('show_2').addEventListener("click", show);
    document.getElementById('type_0').addEventListener("click", type);
    document.getElementById('type_1').addEventListener("click", type);
    document.getElementById('type_2').addEventListener("click", type);
        

    Chart.defaults.font.size = 18;
    Chart.defaults.font.family = 'Vidaloka';
    Chart.defaults.color = '#000';
    
    cols=["#f2ff38",
          "#93d9cc",
          "#f98948"]

    labels=["All data","Class data","My data"];
    
    myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['6:00-8:00','8:00-10:00','10:00-12:00','12:00-14:00','14:00-16:00','16:00-18:00','18:00-20:00','20:00-22:00','22:00-24:00','24:00-2:00','2:00-4:00','4:00-6:00'],
            datasets: [
                {
                    label: "My data",
                    data: data[2],
                    backgroundColor: "#f98948",
                    borderWidth: 2,
                    borderColor: "#000000",
                },
            ],                  
        },
        options: {
            plugins: {
                legend: {
                    display: false
                },
            }, 

            responsive: true,

            scales: {
                y: {
                    beginAtZero: true,
                    suggestedMax: 9000, //find_max(data[0]),
		    max: find_max(data[1])
                },
            },

            transitions: {
                show: {
                    animations: {
                        y: {
                            from: 1000
                        }
                    }
                },
                hide: {
                    animations: {
                        y: {
                            to: 1000
                        }
                    }
                }
            }


            
        }
    });
}


