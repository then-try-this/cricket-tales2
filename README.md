# Cricket tales

A citizen science project where people watch short videos of wild
crickets and tag their behaviour for scientific research purposes.

Includes a new addon version of to provide a teaching resource for KS3
and higher students learning about statistics, evolution and insect
biology.
    
## Setup

Build the python environment
    
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

Make the database

    $ sudo su - postgres
    $ psql
    $ create user "cricket-tales";
    $ alter user "cricket-tales" with encrypted password '<insert password here>';
    $ create database cricket_tales2;
    $ grant all privileges on database cricket_tales2 to "cricket-tales";

Update settings.py with this user.

Moving into django, setup the database via:
    
    $ ./manage.py migrate
    $ ./manage.py createsuperuser

This should now run the test server:
    
    $ ./manage.py runserver

## Backing up and restoring the whole database:

    pg_dump -h localhost -U "cricket-tales" -W cricket_tales2 > ctbak.psql

    psql -h localhost -U "cricket-tales" -d cricket_tales2 <  ctbak.psql

